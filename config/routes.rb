Rails.application.routes.draw do

  scope module: 'shared' do
    resources :account_activations, only: [:edit]
    resources :password_resets, only: [:new, :create, :edit, :update]
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'
    get '/signup', to: 'users#new'
    post '/signup', to: 'users#create'
  end

  namespace :admin do
    root 'pages#index'
    resources :users, except: :new
    resources :countries
    resources :leagues do
      resources :seasons do
        resources :matches, only: :index
      end
      resources :matches
    end
    resources :seasons, only: [:index, :new]
    resources :matches, only: [:index, :new]
    resources :clubs
  end

  scope module: 'site' do
    root 'matches#index'
    get '/search',   to: 'matches#search'
    get '/contacts', to: 'pages#contacts'
    get '/about',    to: 'pages#about'
    get '/:league_slug',              to: 'leagues#matches', as: 'league_matches'
    get '/:league_slug/:season_slug', to: 'seasons#matches', as: 'season_matches',
        season_slug: /\d{4}-\d{4}/
    get '/:league_slug/:season_slug/:club_slug', to: 'clubs#matches',
        as: 'club_matches', season_slug: /\d{4}-\d{4}/
    get '/:league_slug/:match_slug',  to: 'matches#show',  as: 'match'

  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
