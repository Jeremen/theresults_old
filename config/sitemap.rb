# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://theresults.loc"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  add about_path, priority: 0.75, changefreq: nil
  add contacts_path, priority: 0.75, changefreq: nil

  League.find_each do |league|
    add league_matches_path(league), priority: 0.75, changefreq: 'daily',
        lastmod: league.updated_at
  end

  Season.find_each do |season|
    add season_matches_path(season.league, season), priority: 0.75, changefreq: nil,
        lastmod: season.updated_at
  end

  Match.find_each do |match|
    add match_path(match.season.league, match), changefreq: nil, lastmod: match.updated_at
  end
end
