module Site::MatchesHelper
  def home_outcome(match)
    if match.home_score > match.away_score
      :winner
    elsif match.home_score < match.away_score
      :loser
    else
      if match.penalties?
        if match.home_penalty > match.away_penalty
          :winner
        else
          :loser
        end
      end
    end
  end

  def away_outcome(match)
    if match.away_score > match.home_score
      :winner
    elsif match.away_score < match.home_score
      :loser
    else
      if match.penalties?
        if match.away_penalty > match.home_penalty
          :winner
        else
          :loser
        end
      end
    end
  end
end
