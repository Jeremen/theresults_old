class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@theresults.ru'
  layout 'mailer'
end
