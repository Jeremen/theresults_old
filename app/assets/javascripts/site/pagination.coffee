$(document).on "page:change", ->
  if $('#standardPagination').length > 0
    $('#standardPagination').hide()
    loadingMatches = false

    $('#loadMore').show().click ->
      unless loadingMatches
        loadingMatches = true
        moreMatchesUrl = $('.pagination .next_page a').attr('href')
        $this = $(this)
        $this.html('<img src="/assets/ajax-loader.gif" alt="Загрузка..."' +
                   'title="Загрузка...">').addClass('disabled')
        $.getScript moreMatchesUrl, ->
          $this.text('Загрузить ещё').removeClass('disabled') if $this
          loadingMatches = false
      return
