$(document).on "page:change", ->
  perfomingSearch = false
  timerId = null

  $q = $('#q')
  $searchResults = $('#searchResults')

  $q.on "input", ->
    clearTimeout timerId
    $this = $(this)
    timerId = setTimeout ->
      unless perfomingSearch
        perfomingSearch = true
        searchUrl = $this.closest('form').attr('action')
        searchUrl += '?q=' + $this.val()
        $.getScript searchUrl, ->
          perfomingSearch = false

    , 500
    return

  $q.focus ->
    $searchResults.show()

  $q.blur ->
    $searchResults.hide()

  $searchResults.mousedown (e) ->
    e.preventDefault()