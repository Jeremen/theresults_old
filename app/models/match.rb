class Match < ActiveRecord::Base
  searchkick language: 'Russian', word_start: [:search], word_end: [:search]
  belongs_to :home, class_name: 'Club', required: true
  belongs_to :away, class_name: 'Club', required: true
  belongs_to :season, required: true, validate: true, counter_cache: true

  before_validation :generate_title
  before_save :generate_search

  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]

  validates :home_score, numericality: { only_integer: true,
                                         greater_than_or_equal_to: 0,
                                         less_than: 200},
            presence: true
  validates :away_score, numericality: { only_integer: true,
                                         greater_than_or_equal_to: -1,
                                         less_than: 200},
            presence: true
  validates_presence_of :play_date, :home_id, :away_id, :video, :season_id,
                        :user_id, :slug
  validate :home_and_away_cannot_be_same
  validate :home_and_away_penalties_cannot_be_same
  validate :penalties_must_present_both
  validate :if_penalties_present_result_must_be_equal

  def home_and_away_cannot_be_same
    if home_id.present? && away_id.present?
      if home_id == away_id
        errors.add(:home_id, 'не может совпадать с клубом гостем')
      end
    end
  end

  def home_and_away_penalties_cannot_be_same
    if home_penalty.present? && away_penalty.present?
      if home_penalty == away_penalty
        errors.add(:home_penalty, 'не может быть одинаковым с пенальти гостей')
      end
    end
  end

  def penalties_must_present_both
    if (home_penalty.present? || away_penalty.present?) &&
       !(home_penalty.present? && away_penalty.present?)
      if (home_penalty.present?)
        errors.add(:away_penalty, 'должно быть указано')
      else
        errors.add(:home_penalty, 'должно быть указано')
      end
    end
  end

  def if_penalties_present_result_must_be_equal
    if home_penalty.present? && away_penalty.present?
      if home_score != away_score
        errors.add(:home_score, 'должен быть одинаковым с голами гостей
                                 если указаны послематчевые пенальти')
      end
    end
  end

  def penalties?
    home_penalty.present?
  end

  def match_title()
    if penalties?
      "#{home.name} (#{home.city}) - #{away.name} (#{away.city}) " +
      "#{home_score}:#{away_score} " +
      "(#{home_penalty}:#{away_penalty}) - " + play_date.strftime('%d.%m.%Y')
    else
      "#{home.name} (#{home.city}) - #{away.name} (#{away.city}) " +
      "#{home_score}:#{away_score} - " + play_date.strftime('%d.%m.%Y')
    end
  end

  def match_search()
    match_title + " #{season.league.name} - #{season.name}"
  end

  def normalize_friendly_id(string)
    string += ' обзор матча'
    string = string.mb_chars.downcase.to_s
    I18n.transliterate(string.gsub(/:/, '-').gsub(/[()]/, '')).parameterize
  end

  def search_data
    {
      search: search,
      play_date: play_date
    }
  end

  def should_generate_new_friendly_id?
    title_changed?
  end

  private

    def generate_title
      self.title = match_title
    end

    def generate_search
      self.search = match_search
    end
end
