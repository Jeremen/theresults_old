class Club < ActiveRecord::Base
  belongs_to :country, required: true, validate: true, counter_cache: true
  has_many :matches_as_home, class_name: 'Match', foreign_key: :home_id
  has_many :matches_as_away, class_name: 'Match', foreign_key: :away_id

  after_validation {
    retain_specified_errors(%i(name logo))
  }

  after_update :update_matches

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  def slug_candidates
    [
        [:name, :city]
    ]
  end

  validates :name, presence: true,
                   format: { with: /\A[а-яё]+[ а-яё-]*[а-яё]+\z/i,
                             message: 'должно содержать только кириллические
                                       символы, дефисы и пробелы' },
                   uniqueness: { scope: :country_id,
                                 message: 'не может повторяться внутри страны',
                                 case_sensitive: false }
  validates :city, presence: true,
                   format: { with: /\A[а-яё]+[ а-яё-]*[а-яё]+\z/i,
                             message: 'должно содержать только кириллические
                                       символы, дефисы и пробелы' }
  has_attached_file :logo, styles: lambda { |attachment|
    attachment.instance.is_image? ? { original: 'x200' } : {}
  }
  validates_with AttachmentPresenceValidator, attributes: :logo
  validates_with AttachmentContentTypeValidator, attributes: :logo,
                                                 content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :logo,
                                          less_than: 350.kilobytes

  def is_image?
    logo_content_type =~ /\Aimage\/.*\Z/
  end

  def normalize_friendly_id(string)
    string = string.mb_chars.downcase.to_s
    I18n.transliterate(string).parameterize
  end

  def matches(season_id)
    Match.where('season_id = :season_id AND home_id = :id OR away_id = :id',
                season_id: season_id, id: id)
  end

  private

    def retain_specified_errors(attrs_to_retain)
      errors.each do |attr|
        unless attrs_to_retain.include?(attr)
          errors.delete(attr)
        end
      end
    end

    def update_matches
      matches = Match.where('home_id = :id OR away_id = :id', id: id)
      matches.each { |match| match.save }
    end
end
