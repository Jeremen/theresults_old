class Season < ActiveRecord::Base
  belongs_to :league, required: true, validate: true, counter_cache: true
  has_many :matches
  before_save :generate_slug
  after_update :update_matches

  validates :name, presence: true
  validates :start_on, presence: true
  validates :end_on, presence: true
  validates :league_id, presence: true
  validate :start_on_cannot_be_after_end_on

  extend FriendlyId
  friendly_id :slug

  def start_on_cannot_be_after_end_on
    if start_on.present? && end_on.present?
      if start_on > end_on
        errors.add(:start_on, 'не может быть позже конца')
      end
    end
  end

  def matches
    Match.where(season_id: id)
  end

  private

    def generate_slug
      self.slug = "#{start_on.year}-#{end_on.year}"
    end

    def update_matches
      matches = Match.where(season_id: id)
      matches.each { |match| match.save }
    end
end
