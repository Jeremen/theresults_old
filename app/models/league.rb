class League < ActiveRecord::Base
  belongs_to :country, required: true, validate: true, counter_cache: true
  has_many :seasons

  after_validation {
    retain_specified_errors(%i(name logo slug))
  }

  after_update :update_matches

  validates :name,
            format: { with: /\A[[:alnum:]]+[[:alnum:] -]*[[:alnum:]]+\z/i,
                      message: 'должно начинаться и заканчиваться буквой,
                                содержать алфавитно-цифровые символы, дефисы
                                и пробелы' },
            uniqueness: { scope: :country_id,
                          message: 'не может повторяться внутри страны',
                          case_sensitive: false }
  validates_presence_of :name, :country_id, :slug
  has_attached_file :logo, styles: lambda { |attachment|
    attachment.instance.is_image? ? { original: 'x200' } : {}
  }
  validates_with AttachmentPresenceValidator, attributes: :logo
  validates_with AttachmentContentTypeValidator, attributes: :logo,
                                                 content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :logo,
                                          less_than: 350.kilobytes

  extend FriendlyId
  friendly_id :slug

  def matches
    seasons = Season.where(league_id: id).to_a
    Match.where(season_id: seasons)
  end

  def is_image?
    logo_content_type =~ /\Aimage\/.*\Z/
  end

  private

    def retain_specified_errors(attrs_to_retain)
      errors.each do |attr|
        unless attrs_to_retain.include?(attr)
          errors.delete(attr)
        end
      end
    end

    def update_matches
      seasons = Season.where(league_id: id)
      matches = Match.where(season_id: seasons)
      matches.each { |match| match.save }
    end
end
