# TODO add case-insensetive index on database level for name attribute
class Country < ActiveRecord::Base
  has_many :leagues
  has_many :clubs

  after_validation {
    retain_specified_errors(%i(name flag))
  }

  validates :name, presence: true,
                   format: { with: /\A[а-яё]+[ а-яё-]*[а-яё]+\z/i,
                             message: 'должно содержать только кириллические
                                       символы, дефисы и пробелы' },
                   uniqueness: { case_sensitive: false }
  validates_each :name do |record, attr, value|
    record.errors.add(attr, 'должно начинаться с большой буквы') if value =~
        /\A[[:lower:]]/
  end
  has_attached_file :flag, styles: lambda { |attachment|
    attachment.instance.is_image? ? { original: 'x150' } : {}
  }

  validates_with AttachmentPresenceValidator,    attributes: :flag
  validates_with AttachmentContentTypeValidator, attributes: :flag,
                                                 content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator,        attributes: :flag,
                                                 less_than: 350.kilobytes

  def is_image?
    flag_content_type =~ /\Aimage\/.*\Z/
  end

  private

    def retain_specified_errors(attrs_to_retain)
      errors.each do |attr|
        unless attrs_to_retain.include?(attr)
          errors.delete(attr)
        end
      end
    end
end
