# TODO tune receiving emails
# TODO add sitemap gem
# TODO restrict updating of matches by journalist to matches added by himself
# TODO add "empty" legend to index and etc. pages when there are no entities
# TODO sort out with redirection when user try to visit admin panel
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include Shared::SessionsHelper
end
