class Admin::ClubsController < AdminBaseController
  before_action :set_roles
  before_action :require_authorization, except: [:index, :show]

  def index
    @countries = Country.where('clubs_count > 0').order(name: :asc).includes(:clubs)
  end

  def new
    @club = Club.new
    @countries = Country.all.order(name: :asc)
  end

  def create
    @club = Club.new(club_params)
    if @club.save
      flash[:success] = 'Клуб добавлен'
      redirect_to(admin_club_path(@club.id))
    else
      render 'new'
    end
  end

  def show
    @club = Club.find(params[:id])
  end

  def edit
    @club = Club.find(params[:id])
    @countries = Country.all.order(name: :asc)
  end

  def update
    @club = Club.find(params[:id])
    if @club.update(club_params)
      flash[:success] = 'Клуб отредактирован'
      redirect_to(admin_club_path(@club.id))
    else
      render 'edit'
    end
  end

  def destroy
    @club = Club.find(params[:id])
    @club.destroy
    flash[:success] = 'Клуб удален'
    redirect_to request.referrer || admin_root_url
  end

  private

    def set_roles
      @roles = %w(moderator admin)
    end

    # Use strong_parameters for attribute whitelisting
    # Be sure to update your create() and update() controller methods.
    def club_params
      params.require(:club).permit(:name, :country_id, :city, :logo)
    end
end
