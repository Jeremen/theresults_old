class Admin::MatchesController < AdminBaseController
  before_action :set_roles
  before_action :require_authorization

  def index
    if (request.original_url == admin_matches_url)
      @countries = Country.where('leagues_count > 0').includes(leagues: :seasons).
                          where('leagues.seasons_count > 0').references(:leagues).
                          where('seasons.matches_count > 0').references(:seasons)
      path = :admin_league_season_matches_path
      render 'list', locals: { path: path }
    else
      @matches = Match.where('season_id = ?', params[:season_id]).
                      paginate(page: params[:page], per_page: 10).order(created_at: :desc)
      render 'index'
    end
  end

  def new
    @match = Match.new
    if (request.original_url == new_admin_match_url)
      @countries = Country.where('leagues_count > 0').includes(leagues: :seasons)
                          .where('leagues.seasons_count > 0').references(:seasons)
      path = :new_admin_league_match_path
      render 'list', locals: { path: path }
    else
      @league = League.find(params['league_id'])
      @clubs = Club.where('country_id = ?', @league.country_id).order(name: :asc)
      @seasons = Season.where('league_id = ?', @league.id).order(end_on: :desc)
      render 'new'
    end
  end

  def create
    @match = current_user.matches.new(match_params)
    if @match.save
      flash[:success] = 'Матч добавлен'
      redirect_to(admin_league_match_path(@match.season.league.id, @match.id))
    else
      @league = League.find(params['league_id'])
      render 'new'
    end
  end

  def show
    @match = Match.find(params[:id])
  end

  def edit
    @league = League.find(params['league_id'])
    @match = Match.find(params[:id])
    @clubs = Club.where('country_id = ?', @league.country_id).order(name: :asc)
    @seasons = Season.where('league_id = ?', @league.id).order(end_on: :desc)
  end

  def update
    @match = Match.find(params[:id])
    if @match.update(match_params)
      flash[:success] = 'Матч отредактирован'
      redirect_to(admin_league_match_path(@match.season.league.id, @match.id))
    else
      @league = League.find(params['league_id'])
      render 'edit'
    end
  end

  def destroy
    @match = Match.find(params[:id])
    @match.destroy
    flash[:success] = 'Матч удален'
    redirect_to request.referrer || admin_root_url
  end

  private

    def set_roles
      @roles = %w(journalist moderator admin)
    end

    # Use strong_parameters for attribute whitelisting
    # Be sure to update your create() and update() controller methods.
    def match_params
      params.require(:match).permit(:play_date, :home_id, :away_id, :home_score,
                                    :away_score, :video, :season_id, :user_id,
                                    :home_penalty, :away_penalty)
    end
end
