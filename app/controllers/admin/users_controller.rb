class Admin::UsersController < AdminBaseController
  before_action :require_login, except: [:new, :create]
  skip_before_action :require_authorization, only: [:new, :create, :show]

  def index
    @users = User.where.not(id: @current_user.id)
                 .paginate(page: params[:page], per_page: 10).order(created_at: :desc)
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.role = params[:user][:role]
    if @user.save(validate: false)
      flash[:success] = 'Пользователь отредактирован'
      redirect_to([:admin, @user])
    else
      render 'edit'
    end
  end

  def show
  end

  private

    def set_roles
      @roles = %w(admin)
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
