class Admin::LeaguesController < AdminBaseController
  before_action :set_roles
  before_action :require_authorization, except: [:index, :show]

  def index
    @countries = Country.where('leagues_count > 0').order(name: :asc).includes(:leagues)
  end

  def new
    @league = League.new
    @countries = Country.all.order(name: :asc)
  end

  def create
    @league = League.new(league_params)
    if @league.save
      flash[:success] = 'Лига добавлена'
      redirect_to(admin_league_path(@league.id))
    else
      render 'new'
    end
  end

  def show
    @league = League.find(params[:id])
  end

  def edit
    @league = League.find(params[:id])
    @countries = Country.all.order(name: :asc)
  end

  def update
    @league = League.find(params[:id])
    if @league.update(league_params)
      flash[:success] = 'Лига отредактирована'
      redirect_to(admin_league_path(@league.id))
    else
      render 'edit'
    end
  end

  def destroy
    @league = League.find(params[:id])
    @league.destroy
    flash[:success] = 'Лига удалена'
    redirect_to request.referrer || admin_root_url
  end

  private

    def set_roles
      @roles = %w(moderator admin)
    end

    # Use strong_parameters for attribute whitelisting
    # Be sure to update your create() and update() controller methods.
    def league_params
      params.require(:league).permit(:name, :slug, :country_id, :logo)
    end
end
