class Admin::SeasonsController < AdminBaseController
  before_action :set_roles
  before_action :require_authorization, except: [:index, :show]

  def index
    if (request.original_url == admin_seasons_url)
      @countries = Country.where('leagues_count > 0').order(name: :asc).includes(:leagues)
      path = :admin_league_seasons_path
      render 'list', locals: { path: path }
    else
      @seasons = Season.where(league_id: params[:league_id]).order(end_on: :desc).
                        paginate(page: params[:page], per_page: 10)
      render 'index'
    end
  end

  def new
    @season = Season.new
    if (request.original_url == new_admin_season_url)
      @countries = Country.where('leagues_count > 0').order(name: :asc).includes(:leagues)
      path = :new_admin_league_season_path
      render 'list', locals: { path: path }
    else
      @league = League.find(params['league_id'])
      render 'new'
    end
  end

  def create
    @season = Season.new(season_params)
    if @season.save
      flash[:success] = 'Сезон добавлен'
      redirect_to(admin_league_season_path(@season.league.id, @season.id))
    else
      @league = League.find(params['league_id'])
      render 'new'
    end
  end

  def show
    @season = Season.find(params[:id])
  end

  def edit
    @season = Season.find(params[:id])
  end

  def update
    @season = Season.find(params[:id])
    if @season.update(season_params)
      flash[:success] = 'Сезон отредактирован'
      redirect_to(admin_league_season_path(@season.league.id, @season.id))
    else
      render 'edit'
    end
  end

  def destroy
    @season = Season.find(params[:id])
    @season.destroy
    flash[:success] = 'Сезон удален'
    redirect_to request.referrer || admin_root_url
  end

  private

    def set_roles
      @roles = %w(moderator admin)
    end

    # Use strong_parameters for attribute whitelisting
    # Be sure to update your create() and update() controller methods.
    def season_params
      params.require(:season).permit(:name, :start_on, :end_on, :league_id)
    end
end
