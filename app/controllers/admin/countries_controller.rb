class Admin::CountriesController < AdminBaseController
  before_action :set_roles
  before_action :require_authorization, except: [:index, :show]

  def index
    @countries = Country.all.order(name: :asc)
  end

  def new
    @country = Country.new
  end

  def create
    @country = Country.new(country_params)
    if @country.save
      flash[:success] = 'Страна добавлена'
      redirect_to([:admin, @country])
    else
      render 'new'
    end
  end

  def show
    @country = Country.find(params[:id])
  end

  def edit
    @country = Country.find(params[:id])
  end

  def update
    @country = Country.find(params[:id])
    if @country.update(country_params)
      flash[:success] = 'Страна отредактирована'
      redirect_to([:admin, @country])
    else
      render 'edit'
    end
  end

  def destroy
    @country = Country.find(params[:id])
    @country.destroy
    flash[:success] = 'Страна удалена'
    redirect_to request.referrer || admin_root_url
  end

  private

    def set_roles
      @roles = %w(moderator admin)
    end

    # Use strong_parameters for attribute whitelisting
    # Be sure to update your create() and update() controller methods.
    def country_params
      params.require(:country).permit(:name, :flag)
    end
end
