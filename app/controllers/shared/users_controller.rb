class Shared::UsersController < ApplicationController

  def new
    @user = User.new
    render layout: 'not_logged_in'
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Пожалуйста проверьте почту для активации вашей учетной
                      записи'
      redirect_to signup_path
    else
      render 'new', layout: 'not_logged_in'
    end
  end

  private

    def user_params
      params.require(:user)
        .permit(:name, :email, :password, :password_confirmation)
    end
end
