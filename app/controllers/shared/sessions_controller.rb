class Shared::SessionsController < ApplicationController
  layout 'not_logged_in'

  def new
    if logged_in?
      redirect_to root_path
    end
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or admin_root_path
      else
        message = 'Учетная запись не активирована.'
        message += 'Проверьте электронную почту на наличие ссылки для
                    активации.'
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Комбинация с такой электронной почтой и паролем
                            не найдена.'
      render 'new'
    end

  end

  def destroy
    log_out if logged_in?
    redirect_to login_path
  end
end
