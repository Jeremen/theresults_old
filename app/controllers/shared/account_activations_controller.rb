# TODO  after adding signup ability to all users add redirect to their profile
class Shared::AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = 'Учетная запись успешно активирована!'
      #redirect_to user
      redirect_to root_url
    else
      flash[:danger] = 'Некорректная ссылка для активакции'
      redirect_to root_url
    end
  end
end
