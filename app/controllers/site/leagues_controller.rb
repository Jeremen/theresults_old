class Site::LeaguesController < ApplicationController
  def matches
    @league = League.friendly.find(params[:league_slug])
    @matches = @league.matches.paginate(page: params[:page], per_page: 10).
                              order(created_at: :desc)
    @last_seasons = Season.where.not(league_id: @league.id)
                          .select('DISTINCT ON (league_id) *').order(:league_id, end_on: :desc)
    @seasons = Season.where(league_id: @league.id).order(end_on: :desc)
  end
end
