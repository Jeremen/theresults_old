class Site::ClubsController < ApplicationController
  def matches
    @league  = League.friendly.find(params[:league_slug])
    @season  = @league.seasons.friendly.find(params[:season_slug])
    @last_seasons = Season.where.not(league_id: @league.id).select('DISTINCT ON (league_id) *').
                          order(:league_id, end_on: :desc)
    @seasons = Season.where(league_id: @league.id).where.not(id: @season.id).
                     order(end_on: :desc)
    home_club_ids = Match.where(season_id: @season.id).select(:home_id).distinct.pluck(:home_id)
    away_club_ids = Match.where(season_id: @season.id).select(:away_id).distinct.pluck(:away_id)
    club_ids = home_club_ids + away_club_ids
    @club = Club.friendly.find(params[:club_slug])
    @clubs = Club.where(id: club_ids.uniq).where.not(id: @club.id).order(name: :asc)
    @matches = @club.matches(@season.id).paginate(page: params[:page], per_page: 10).
                    order(created_at: :desc)
  end
end
