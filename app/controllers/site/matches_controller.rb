class Site::MatchesController < ApplicationController

  def index
    @matches = Match.paginate(page: params[:page], per_page: 10).
                    order(created_at: :desc)
    @last_seasons = Season.select('DISTINCT ON (league_id) *').
                          order(:league_id, end_on: :desc)
  end

  def show
    @match = Match.friendly.find(params[:match_slug])
    if request.path != match_path(@match.season.league.slug, @match)
      redirect_to match_path(@match.season.league.slug, @match),
                  status: :moved_permanently
    end
  end

  def search
    @search_results = Match.search(params[:q], fields: [{search: :word_start},
                                                        {search: :word_end}],
                                   order: {play_date: :desc}, limit: 6)
  end
end
