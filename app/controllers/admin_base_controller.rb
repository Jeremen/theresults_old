class AdminBaseController < ApplicationController
  before_action :require_login
  before_action :set_roles
  before_action :require_authorization

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout 'admin'

  private

    def require_login
      unless logged_in?
        store_location
        flash[:danger] = 'Вам нужно войти чтобы получить доступ к этой секции'
        redirect_to login_url
      end
    end

    def set_roles
      @roles = %w(journalist moderator admin)
    end

    def require_authorization()
      unless @roles.include?(current_user.role)
        flash[:danger] = 'У вас недостаточно привилегий для совершения данного
                          действия'
        redirect_to request.referrer || admin_root_url
      end
    end
end
