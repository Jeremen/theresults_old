# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150904121343) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clubs", force: :cascade do |t|
    t.string   "name",              null: false
    t.integer  "country_id",        null: false
    t.string   "city",              null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "slug"
  end

  add_index "clubs", ["country_id"], name: "index_clubs_on_country_id", using: :btree
  add_index "clubs", ["slug"], name: "index_clubs_on_slug", unique: true, using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "flag_file_name"
    t.string   "flag_content_type"
    t.integer  "flag_file_size"
    t.datetime "flag_updated_at"
    t.integer  "leagues_count",     default: 0, null: false
    t.integer  "clubs_count",       default: 0, null: false
  end

  add_index "countries", ["name"], name: "index_countries_on_name", unique: true, using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "leagues", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id",                    null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "slug",                          null: false
    t.integer  "seasons_count",     default: 0, null: false
  end

  add_index "leagues", ["country_id"], name: "index_leagues_on_country_id", using: :btree
  add_index "leagues", ["slug"], name: "index_leagues_on_slug", unique: true, using: :btree

  create_table "matches", force: :cascade do |t|
    t.date     "play_date",    null: false
    t.integer  "home_id",      null: false
    t.integer  "away_id",      null: false
    t.integer  "season_id",    null: false
    t.integer  "home_score"
    t.integer  "away_score"
    t.string   "video",        null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
    t.integer  "home_penalty"
    t.integer  "away_penalty"
    t.string   "title",        null: false
    t.string   "slug",         null: false
    t.string   "search",       null: false
  end

  add_index "matches", ["away_id"], name: "index_matches_on_away_id", using: :btree
  add_index "matches", ["home_id"], name: "index_matches_on_home_id", using: :btree
  add_index "matches", ["season_id"], name: "index_matches_on_season_id", using: :btree
  add_index "matches", ["slug"], name: "index_matches_on_slug", unique: true, using: :btree
  add_index "matches", ["user_id"], name: "index_matches_on_user_id", using: :btree

  create_table "seasons", force: :cascade do |t|
    t.string   "name",                      null: false
    t.date     "start_on",                  null: false
    t.date     "end_on",                    null: false
    t.integer  "league_id",                 null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "slug",                      null: false
    t.integer  "matches_count", default: 0, null: false
  end

  add_index "seasons", ["league_id", "slug"], name: "index_seasons_on_league_id_and_slug", unique: true, using: :btree
  add_index "seasons", ["league_id"], name: "index_seasons_on_league_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "password_digest"
    t.string   "role",              default: "user", null: false
    t.string   "remember_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "clubs", "countries"
  add_foreign_key "leagues", "countries"
  add_foreign_key "matches", "clubs", column: "away_id"
  add_foreign_key "matches", "clubs", column: "home_id"
  add_foreign_key "matches", "seasons"
  add_foreign_key "matches", "users"
  add_foreign_key "seasons", "leagues"
end
