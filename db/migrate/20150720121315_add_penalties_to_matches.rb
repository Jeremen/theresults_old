class AddPenaltiesToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :home_penalty, :integer, default: nil
    add_column :matches, :away_penalty, :integer, default: nil
  end
end
