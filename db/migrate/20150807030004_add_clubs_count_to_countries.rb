class AddClubsCountToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :clubs_count, :integer, null: false, default: 0
  end
end
