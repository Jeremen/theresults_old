class AddLeaguesCountToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :leagues_count, :integer, null: false, default: 0
  end
end
