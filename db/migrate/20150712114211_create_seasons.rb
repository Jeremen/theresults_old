class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.string :name, null: false
      t.date :start, null: false
      t.date :end, null: false
      t.references :league, index: true, null: false, foreign_key: true

      t.timestamps null: false
    end
  end
end
