class AddSeasonsCountToLeagues < ActiveRecord::Migration
  def change
    add_column :leagues, :seasons_count, :integer, null: false, default: 0
  end
end
