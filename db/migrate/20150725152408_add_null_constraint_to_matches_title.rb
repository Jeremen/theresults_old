class AddNullConstraintToMatchesTitle < ActiveRecord::Migration
  def change
    change_column_null :matches, :title, false
  end
end
