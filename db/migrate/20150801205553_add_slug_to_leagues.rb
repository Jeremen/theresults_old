class AddSlugToLeagues < ActiveRecord::Migration
  def change
    add_column :leagues, :slug, :string, null: false
    add_index :leagues, :slug, unique: true
  end
end
