class AddSearchFieldToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :search, :string, null: false
  end
end
