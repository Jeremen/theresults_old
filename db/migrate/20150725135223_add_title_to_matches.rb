class AddTitleToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :title, :string, index: true
  end
end
