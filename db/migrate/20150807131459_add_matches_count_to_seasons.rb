class AddMatchesCountToSeasons < ActiveRecord::Migration
  def change
    add_column :seasons, :matches_count, :integer, null: false, default: 0
  end
end
