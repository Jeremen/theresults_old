class AddSlugToSeasons < ActiveRecord::Migration
  def change
    add_column :seasons, :slug, :string, null: false
    add_index :seasons, [:league_id, :slug], unique: true
  end
end
