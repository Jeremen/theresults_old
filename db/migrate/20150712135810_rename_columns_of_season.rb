class RenameColumnsOfSeason < ActiveRecord::Migration
  def change
    rename_column :seasons, :start, :started_on
    rename_column :seasons, :end, :ended_on
  end
end
