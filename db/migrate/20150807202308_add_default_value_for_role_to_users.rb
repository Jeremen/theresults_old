class AddDefaultValueForRoleToUsers < ActiveRecord::Migration
  def change
    change_column_default :users, :role, 'user'
    change_column_null :users, :role, false, 'user'
  end
end
