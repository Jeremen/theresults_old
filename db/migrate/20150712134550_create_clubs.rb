class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name, null: false
      t.references :country, index: true, null: false, foreign_key: true
      t.string :city, null: false

      t.timestamps null: false
    end
  end
end
