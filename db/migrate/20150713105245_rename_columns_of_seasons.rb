class RenameColumnsOfSeasons < ActiveRecord::Migration
  def change
    rename_column :seasons, :started_on, :start_on
    rename_column :seasons, :ended_on, :end_on
  end
end
