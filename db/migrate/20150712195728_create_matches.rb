class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.date :play_date, null: false
      t.integer :home_id, index: true, null: false
      t.integer :away_id, index: true, null: false
      t.references :season, index: true, null: false, foreign_key: true
      t.integer :home_score
      t.integer :away_score
      t.string :video, null: false

      t.timestamps null: false
    end
    add_foreign_key :matches, :clubs, column: :home_id
    add_foreign_key :matches, :clubs, column: :away_id
  end
end
